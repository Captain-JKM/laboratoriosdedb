import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class RDBMS {
  public static void main(String[] args) {
    Conex conectDB = new Conex ();
    Connection connection = conectDB.Conectardb ();

    try {
      MedicoDAO medicoDAO = new MedicoDAO (connection);

      List<Medico> medicos = medicoDAO.getMedicos ();

      System.out.println ("Lista de Médicos:");
      for (Medico medico : medicos) {
        System.out.println ("ID: " + medico.getId ());
        System.out.println ("Nombre Completo: " + medico.getNombreCompleto ());
        System.out.println ("Especialidad: " + medico.getEspecialidad ());
        System.out.println ("Número Colegiado: " + medico.getNumeroColegiado ());
        System.out.println ("Años de Experiencia: " + medico.getAnosExperiencia ());
        System.out.println ("Horario de Atención: " + medico.getHorarioAtencion ());
        System.out.println ("Consultorio: " + medico.getConsultorio ());
        System.out.println ();
      }
    } catch (SQLException e) {
      e.printStackTrace ();
      System.out.println ("Error no hay medico: " + e.getMessage ());
    }
/*
    try {
      MedicoDAO medicoDAO = new MedicoDAO (connection);


      Medico nuevoMedico = new Medico ();
      nuevoMedico.setNombreCompleto ("Dr. Marcus Feniz");
      nuevoMedico.setEspecialidad ("Cirujano");
      nuevoMedico.setNumeroColegiado ("9856");
      nuevoMedico.setAnosExperiencia ("20");
      nuevoMedico.setHorarioAtencion ("Lunes a viernes de 8:00 a 14:00");
      nuevoMedico.setConsultorio ("Consulta 104");


      medicoDAO.insertMedico (nuevoMedico);

      System.out.println ("Medico Agregado con éxito.:");

    } catch (SQLException e) {
      e.printStackTrace ();
      System.out.println ("Error al insertar médico: " + e.getMessage ());
    }
  }



    try {
      MedicoDAO medicoDAO = new MedicoDAO(connection);

      String idMedicoEliminar = "3";

      medicoDAO.deleteMedico(idMedicoEliminar);

      System.out.println("Médico + eliminado correctamente.");

    } catch (SQLException e) {
      e.printStackTrace();
      System.out.println("Error al eliminar médico: " + e.getMessage());
    }
     }


    try {
      MedicoDAO medicoDAO = new MedicoDAO (connection);

      String idMedicoActualizar = "4";

      Medico medicoActualizado = new Medico ();

      String medicoUpdate = "Dr. Marcus Fenix";
      medicoActualizado.setNombreCompleto (medicoUpdate);
      medicoActualizado.setEspecialidad("Cirujano");
      medicoActualizado.setNumeroColegiado ("9856");
      medicoActualizado.setAnosExperiencia ("20");
      medicoActualizado.setHorarioAtencion ("Lunes a viernes de 8:00 a 14:00");
      medicoActualizado.setConsultorio ("Consulta 104");

      medicoActualizado.setNombreCompleto (medicoUpdate);

      medicoDAO.updateMedico (idMedicoActualizar, medicoActualizado);

      System.out.println ("Médico ".concat (medicoUpdate).concat (" Se ha actualizado correctamente!"));

    } catch (SQLException e) {
      e.printStackTrace ();
      System.out.println ("Error al actualizar médico: " + e.getMessage ());
    }
 */

    try {
      MedicoDAO medicoDAO = new MedicoDAO (connection);

      String idMedico = "5";
      medicoDAO.deleteMedico (idMedico);

      System.out.println ("Se elimino el médico con ID: ".concat (idMedico));


    } catch (SQLException e){
      e.printStackTrace ();
      System.out.println ("No es posible eliminar el médico");
    }


  }
}