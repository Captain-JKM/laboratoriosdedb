public class Medico {
  private String id;
  private String nombreCompleto;
  private String especialidad;
  private String numeroColegiado;
  private String anosExperiencia;
  private String horarioAtencion;
  private String consultorio;

  public String getId() {
    return  id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNombreCompleto() {
    return nombreCompleto;
  }

  public void setNombreCompleto(String nombreCompleto) {
    this.nombreCompleto = nombreCompleto;
  }

  public String getEspecialidad() {
    return especialidad;
  }

  public void setEspecialidad(String especialidad) {
    this.especialidad = especialidad;
  }

  public String getNumeroColegiado() {
    return numeroColegiado;
  }

  public void setNumeroColegiado(String numeroColegiado) {
    this.numeroColegiado = numeroColegiado;
  }

  public String getAnosExperiencia() {
    return anosExperiencia;
  }

  public void setAnosExperiencia(String anosExperiencia) {
    this.anosExperiencia = anosExperiencia;
  }

  public String getHorarioAtencion() {
    return horarioAtencion;
  }

  public void setHorarioAtencion(String horarioAtencion) {
    this.horarioAtencion = horarioAtencion;
  }

  public String getConsultorio() {
    return consultorio;
  }

  public void setConsultorio(String consultorio) {
    this.consultorio = consultorio;
  }
}


