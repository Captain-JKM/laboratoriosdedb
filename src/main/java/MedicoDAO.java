import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MedicoDAO extends JFrame {
  private Connection connection;

  public MedicoDAO(Connection connection) {
    this.connection = connection;
  }

  public void insertMedico(Medico medico) throws SQLException {
    String querySQL = "INSERT INTO Medico (NombreCompleto, Especialidad, NumeroColegiado, AnosExperiencia, HorarioAtencion, Consultorio) " +
            "VALUES (?, ?, ?, ?, ?, ?)";

    try (PreparedStatement statement = connection.prepareStatement(querySQL)) {
      statement.setString(1, medico.getNombreCompleto());
      statement.setString(2, medico.getEspecialidad());
      statement.setString(3, medico.getNumeroColegiado());
      statement.setString(4, medico.getAnosExperiencia());
      statement.setString(5, medico.getHorarioAtencion());
      statement.setString(6, medico.getConsultorio());

      statement.executeUpdate();
    }
  }



  public List<Medico> getMedicos() throws SQLException {
    List<Medico> medicos = new ArrayList<> ();
    String querySQL = "SELECT * FROM Medico";

    try (PreparedStatement statement = connection.prepareStatement (querySQL)) {
      try (ResultSet resultSet = statement.executeQuery()) {
        while (resultSet.next()) {
          Medico medico = new Medico();
          medico.setId(resultSet.getString ("IDMedico"));
          medico.setNombreCompleto(resultSet.getString("NombreCompleto"));
          medico.setEspecialidad(resultSet.getString("Especialidad"));
          medico.setNumeroColegiado(resultSet.getString("NumeroColegiado"));
          medico.setAnosExperiencia(resultSet.getString ("AnosExperiencia"));
          medico.setHorarioAtencion(resultSet.getString("HorarioAtencion"));
          medico.setConsultorio(resultSet.getString("Consultorio"));
          medicos.add(medico);
        }
      }
    }
    return medicos;
  }

  public void updateMedico(String id, Medico medico) throws SQLException {
    String querySQL = "UPDATE Medico SET NombreCompleto=?, Especialidad=?, NumeroColegiado=?, AnosExperiencia=?, HorarioAtencion=?, Consultorio=? WHERE IDMedico=?";

    try (PreparedStatement statement = connection.prepareStatement(querySQL)) {
      statement.setString(1, medico.getNombreCompleto());
      statement.setString(2, medico.getEspecialidad());
      statement.setString(3, medico.getNumeroColegiado());
      statement.setString(4, medico.getAnosExperiencia());
      statement.setString(5, medico.getHorarioAtencion());
      statement.setString(6, medico.getConsultorio());
      statement.setString(7, id);

      statement.executeUpdate();
    }
  }


  public void deleteMedico(String id) throws SQLException {
    String querySQL = "DELETE FROM Medico WHERE IDMedico=?";

    try (PreparedStatement statement = connection.prepareStatement(querySQL)) {
      statement.setString(1, id);
      statement.executeUpdate();
    }
  }




}

