import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conex {
  public Connection Conectardb() {
    Connection connection = null;
    String dbName = "hospital";
    String dbURL = "jdbc:mysql://localhost/"+dbName;
    String dbUser = "root"; //Modifica tus credenciales.
    String dbPassword = "";

    System.out.println("Conectando a ".concat (dbName));
    try {
      connection = DriverManager.getConnection(dbURL, dbUser, dbPassword);
      System.out.println("Conexión exitosa a".concat (dbName));
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new RuntimeException(e);
    }
    return connection;
  }
}
