USE hospital;

INSERT INTO Paciente (NombreCompleto, FechaNacimiento, Genero, Direccion, Telefono, NumeroSeguroMedico, HistorialMedico, Alergias, TipoSangre)
VALUES
('Ana García López', '1980-01-01', 'F', 'Calle Mayor 123', '123456789', '123456789A', 'Sin historial médico', 'Ninguna', 'A+'),
('Juan Pérez Martínez', '1990-02-02', 'M', 'Calle del Sol 456', '987654321', '987654321B', 'Hipertensión arterial', 'Penicilina', 'AB+'),
('María González Rodríguez', '2000-03-03', 'F', 'Calle de la Luna 789', '112233445', '112233445C', 'Asma bronquial', 'Polen', '0+');

INSERT INTO Medico (NombreCompleto, Especialidad, NumeroColegiado, AnosExperiencia, HorarioAtencion, Consultorio)
VALUES
('Dr. Pedro López', 'Cardiología', '12345', 10, 'Lunes a viernes de 9:00 a 13:00', 'Consulta 101'),
('Dra. Ana Martín', 'Pediatría', '23456', 5, 'Lunes a viernes de 14:00 a 18:00', 'Consulta 102'),
('Dr. Juan García', 'Traumatología', '34567', 20, 'Lunes a viernes de 8:00 a 12:00', 'Consulta 103');

INSERT INTO Cita (IDCita, FechaHora, MotivoCita, EstadoCita, IDPaciente, IDMedico)
VALUES
(1, '2023-11-14 10:00', 'Revisión médica anual', 'Confirmada', 1, 1),
(2, '2023-11-15 11:00', 'Control de la tensión arterial', 'Confirmada', 2, 1),
(3, '2023-11-16 12:00', 'Vacunación infantil', 'Confirmada', 3, 2);

INSERT INTO HistoriaClinica (IDHistoriaClinica, FechaCreacion, NotasMedico, Diagnosticos, ResultadosPruebas, Prescripciones, IDPaciente)
VALUES
(1, '2023-11-14', 'Paciente asintomática. Se realiza revisión médica anual.', 'Sin diagnósticos', 'No se realizan pruebas', 'No se prescriben medicamentos', 1),
(2, '2023-11-15', 'Paciente presenta tensión arterial elevada. Se controla la tensión arterial y se ajusta la medicación.', 'Hipertensión arterial', 'Tensión arterial: 140/90 mmHg', 'Losartan 100 mg/día', 2),
(3, '2023-11-16', 'Paciente presenta crisis de asma. Se administra broncodilatador y se pauta tratamiento preventivo.', 'Asma bronquial', 'Espirometría: FEV1 80% del valor predicho', 'Salbutamol inhalador 2 puffs cada 4 horas', 3);

INSERT INTO Prueba (IDPrueba, TipoPrueba, Descripcion, Resultados, FechaRealizacion, IDPaciente)
VALUES
(1, 'Análisis de sangre', 'Hemograma completo, glucosa, creatinina, colesterol', 'Hemograma normal, glucosa 100 mg/dL, creatinina 1 mg/dL, colesterol 200 mg/dL', '2023-11-14', 1),
(2, 'Toma de presión arterial', 'Medición de la presión arterial en ambos brazos', 'Presión arterial: 140/90 mmHg', '2023-11-15', 2),
(3, 'Espirometría', 'Medición del volumen de aire que los pulmones pueden expulsar', 'FEV1 80% del valor predicho', '2023-11-16', 3),
(4, 'Electrocardiograma', 'Registro de la actividad eléctrica del corazón', 'Fibrilación auricular', '2023-11-17', 2);

INSERT INTO Medicamento (IDMedicamento, NombreMedicamento, Presentacion, Dosis, Indicaciones, Contraindicaciones)
VALUES
(1, 'Paracetamol', 'Comprimidos de 500 mg', '1 comprimido cada 4 horas', 'Dolor leve a moderado', 'Alergia al paracetamol, enfermedad hepática grave'),
(2, 'Losartan', 'Comprimidos de 100 mg', '1 comprimido al día', 'Hipertensión arterial', 'Hipotensión, angioedema, insuficiencia renal'),
(3, 'Salbutamol', 'Inhalador de 100 mcg/dosis', '2 inhalaciones cada 4 horas', 'Asma bronquial, bronquitis crónica', 'Taquicardia, arritmias cardíacas, temblor'),
(4, 'Amiodarona', 'Comprimidos de 200 mg', '1 comprimido cada 12 horas', 'Arritmias cardíacas', 'Bradicardia, hipotensión, daño pulmonar'),
(5, 'Ibuprofeno', 'Comprimidos de 400 mg', '1 comprimido cada 6 horas', 'Fiebre, dolor leve a moderado', 'Alergia al ibuprofeno, úlcera péptica, enfermedad renal');

INSERT INTO Receta (IDReceta, FechaCreacion, IDMedicamento, Dosis, DuracionTratamiento, IDMedico, IDPaciente)
VALUES
(1, '2023-11-14', 1, '1 comprimido al día', 'Durante 1 mes', 1, 1),
(2, '2023-11-14', 2, '1 comprimido cada 8 horas', 'Durante 3 meses', 1, 1),
(3, '2023-11-15', 3, '2 inhalaciones cada 4 horas', 'Durante 1 semana', 2, 2),
(4, '2023-11-16', 4, '1 comprimido cada 12 horas', 'Durante 2 meses', 1, 3),
(5, '2023-11-18', 5, 'Paracetamol 500 mg cada 4 horas', 'Según necesidad', 2, 2);
