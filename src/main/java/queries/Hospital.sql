-- Creación de la base de datos
DROP DATABASE IF EXISTS hospital;
CREATE DATABASE hospital;
USE hospital;

-- Tabla Paciente
CREATE TABLE Paciente (
    IDPaciente INT AUTO_INCREMENT PRIMARY KEY,
    NombreCompleto VARCHAR(255) NOT NULL,
    FechaNacimiento DATE NOT NULL,
    Genero CHAR(1) NOT NULL,
    Direccion VARCHAR(255) NOT NULL,
    Telefono VARCHAR(20) NOT NULL,
    NumeroSeguroMedico VARCHAR(20) NOT NULL,
    HistorialMedico TEXT,
    Alergias VARCHAR(255),
    TipoSangre VARCHAR(3)
);

-- Tabla Medico
CREATE TABLE Medico (
    IDMedico INT AUTO_INCREMENT PRIMARY KEY,
    NombreCompleto VARCHAR(255) NOT NULL,
    Especialidad VARCHAR(50) NOT NULL,
    NumeroColegiado VARCHAR(20) NOT NULL,
    AnosExperiencia INT NOT NULL,
    HorarioAtencion VARCHAR(255) NOT NULL,
    Consultorio VARCHAR(20) NOT NULL
);

-- Tabla Cita
CREATE TABLE Cita (
    IDCita INT AUTO_INCREMENT PRIMARY KEY,
    FechaHora DATETIME NOT NULL,
    MotivoCita VARCHAR(255) NOT NULL,
    EstadoCita VARCHAR(10) NOT NULL,
    IDPaciente INT NOT NULL,
    IDMedico INT NOT NULL,
    FOREIGN KEY (IDPaciente) REFERENCES Paciente(IDPaciente),
    FOREIGN KEY (IDMedico) REFERENCES Medico(IDMedico)
);

-- Tabla HistoriaClinica
CREATE TABLE HistoriaClinica (
    IDHistoriaClinica INT AUTO_INCREMENT PRIMARY KEY,
    FechaCreacion DATE NOT NULL,
    NotasMedico TEXT,
    Diagnosticos VARCHAR(255),
    ResultadosPruebas TEXT,
    Prescripciones TEXT,
    IDPaciente INT NOT NULL,
    FOREIGN KEY (IDPaciente) REFERENCES Paciente(IDPaciente)
);

-- Tabla Prueba
CREATE TABLE Prueba (
    IDPrueba INT AUTO_INCREMENT PRIMARY KEY,
    TipoPrueba VARCHAR(50) NOT NULL,
    Descripcion VARCHAR(255) NOT NULL,
    Resultados VARCHAR(255) NOT NULL,
    FechaRealizacion DATE NOT NULL,
    IDPaciente INT NOT NULL,
    FOREIGN KEY (IDPaciente) REFERENCES Paciente(IDPaciente)
);

-- Tabla Medicamento
CREATE TABLE Medicamento (
    IDMedicamento INT AUTO_INCREMENT PRIMARY KEY,
    NombreMedicamento VARCHAR(255) NOT NULL,
    Presentacion VARCHAR(50) NOT NULL,
    Dosis VARCHAR(50) NOT NULL,
    Indicaciones VARCHAR(255) NOT NULL,
    Contraindicaciones VARCHAR(255) NOT NULL
);

-- Tabla Receta
CREATE TABLE Receta (
    IDReceta INT AUTO_INCREMENT PRIMARY KEY,
    FechaCreacion DATE NOT NULL,
    IDMedicamento INT NOT NULL,
    Dosis VARCHAR(50) NOT NULL,
    DuracionTratamiento VARCHAR(100) NOT NULL,
    IDMedico INT NOT NULL,
    IDPaciente INT NOT NULL,
    FOREIGN KEY (IDMedicamento) REFERENCES Medicamento(IDMedicamento),
    FOREIGN KEY (IDMedico) REFERENCES Medico(IDMedico),
    FOREIGN KEY (IDPaciente) REFERENCES Paciente(IDPaciente)
);
